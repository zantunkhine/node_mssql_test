
// using express
const express = require('express');
const app = express();
const sql = require('mssql/msnodesqlv8')

// var dbConfig = require("./dbConfig")

// //Middlewares
// app.use('/item2', async () => {
//     //connecting to database

//     try {
//         // make sure that any items are correctly URL encoded in the connection string
//         await sql.connect(dbConfig, async () => {
//             //const result = await sql.query ('select * from States_Capital')
//             console.log('connected')
//         })

//     } catch (err) {
//         console.log(err)
//     }
// })

// //ROUTES
// app.get('/', (req, res) => {
//     res.send('We are on the HOME Page!');
// });


// app.get('/item2', (req, res) => {
//     res.send('We are on item2!');
// });

// // Start listening to the server
// app.listen(3000, () => {
//     console.log("Server is running!")
// });




// using HTTP

// const http = require('http');
// const hostname = '127.0.0.1'
// const port = 3000

// const server = http.createServer((req,res)=>{
//     res.statusCode = 200
//     res.setHeader('content-type','text/plain')
//     res.end('Hello World!\n')
// })

// server.listen(port,hostname,()=>{
//     console.log(`Server running at http://${hostname}:${port}/`)
// })



app.get('/', function (req, res) {
   

    // config for your database
    var config= {
        user: 'ZTK\\Zan Tun Khine',
        server: 'localhost', 
        driver: "msnodesqlv8",
        database: 'Myanmar',
        options: {
            trustedConnection: true,
            encrypt:false,
            enableArithAbort:true  
          }
            
    };

    // connect to your database
    sql.connect(config, function (err) {
    
        if (err) console.log(err);
        // create Request object
        var request = new sql.Request();
           
        // query to the database and get the records
        request.query('select * from States_Capital', function (err, recordset) {
            
            if (err) console.log(err)

            // send records as a response
            res.send(recordset);
            
        });
    });
});

var server = app.listen(5000, function () {
    console.log('Server is running..');
});